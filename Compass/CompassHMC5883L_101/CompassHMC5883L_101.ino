#include <Wire.h>
#define address 0x1E

void setup(){
  Serial.begin(9600);
  delay(100);
  Wire.begin();
  
  Wire.beginTransmission(address);
  Wire.write(0x02);
  Wire.write(0x00);
  Wire.endTransmission();
}

void loop(){
  int x,y,z;
  
  //Telling compass to begin reading data
  Wire.beginTransmission(address);
  Wire.write(0x03);
  Wire.endTransmission();
  
  //read data from each axis, 2 registers each
  Wire.requestFrom(address,6);
  if(Wire.available()){
    x =Wire.read()<<8;
    x |= Wire.read();
    
    z =Wire.read()<<8;
    z |= Wire.read();
    
    y=Wire.read()<<8;
    y |= Wire.read();
  }
  
  Serial.print("x:");
  Serial.print(x);
  
  Serial.print("y:");
  Serial.print(y);
  
  Serial.print("z:");
  Serial.println(z);
  
  delay(250);
}
