#include<Servo.h>
#define MINSPEED 1000
#define MAXSPEED 2000

#define m1 5
#define m2 6
#define m3 9
#define m4 10

Servo motor1,motor2,motor3,motor4;

int i, good, k;
byte data_in;
int danger_flag=0;
int flag=0;

int tspeed,fspeed,bspeed,rspeed,lspeed;

void setup(){
  Serial.begin(115200);
  Serial.println("S");
  motor1.attach(m1);
  motor2.attach(m2);
  motor3.attach(m3);
  motor4.attach(m4);
  warnme();
  pinMode(13,OUTPUT);
  delay(2000);
  
  pinMode(3, INPUT);
  pinMode(13, OUTPUT);
  
  delay(1);
  attachInterrupt(1,data_incoming,RISING);
    motor1.writeMicroseconds(MAXSPEED);
  motor2.writeMicroseconds(MAXSPEED);
  motor3.writeMicroseconds(MAXSPEED);
  motor4.writeMicroseconds(MAXSPEED);

  // Wait for input
  delay(6000);
  // Send min output
  Serial.println("Sending minimum output");
  motor1.writeMicroseconds(MINSPEED);
  motor2.writeMicroseconds(MINSPEED);
  motor3.writeMicroseconds(MINSPEED);
  motor4.writeMicroseconds(MINSPEED);
  
}//setup

void loop(){
  
  
}//loop

void data_incoming(){
  detachInterrupt(1);
//  delay(1);
    for(i=0; i<100; i++){
      delayMicroseconds(20);
      good=1;
      if(digitalRead(3)==LOW){
      good=0;
      i=100;
      }
    }
      
    if(good==1){
   
    while(1){
      if(digitalRead(3)==LOW){
        delayMicroseconds(750);

        for(i=0; i<8; i++){
          if(digitalRead(3)==HIGH)
          bitWrite(data_in, i, 1);
          else
          bitWrite(data_in, i, 0);
          delayMicroseconds(1000);
        }//for
        if(data_in=='#')
        Serial.println("");
        else
        Serial.print(char(data_in));

       break;//secondtwhile
      }//low kickoff
      
    }//second while
    
    }//good check

  
  //======= HANDLE MOTORS
  /*
      START - 'O'
      STOP - 'X'
      FRONT - 'F'
      BACK - 'B'
      RIGHT - 'R'
      LEFT - 'L'
      THROTTLE - 'T'
      BRAKE - 'E'
   */
  
  char recvd;
  recvd=(char)data_in;
  
  if(recvd=='O'){
    danger_flag=-1;
  }
  else if(recvd=='X'){
    danger_flag=1;
  }
  
  if(danger_flag==-1){ // safe to lift
    if(flag==0){
      fspeed=MINSPEED;
      bspeed=MINSPEED;
      rspeed=MINSPEED;
      lspeed=MINSPEED;
      write_motors();
      flag=1;
    }
    
    //==THROTTLE OR BRAKE
    if(recvd=='T'){
      fspeed+=10;
      bspeed+=10;
      rspeed+=10;
      lspeed+=10;
      write_motors();
    }
    else if(recvd=='E'){
      fspeed-=10;
      bspeed-=10;
      rspeed-=10;
      lspeed-=10;
      write_motors();
    }
    
    //==FRONT OR BACK
    if(recvd=='F'){
      fspeed-=25;
      bspeed-=25;
      rspeed+=25;
      lspeed+=25;
      write_motors();
    }
    else if(recvd=='B'){
      fspeed+=25;
      bspeed+=25;
      rspeed-=25;
      lspeed-=25;
      write_motors();
    }
//    else{
////      tspeed=(int)((fspeed+bspeed+rspeed+lspeed)/4);
////      fspeed=tspeed;
////      bspeed=tspeed;
////      rspeed=tspeed;
////      lspeed=tspeed;
////      write_motors();
//    }
    
    //==RIGHT OR LEFT
    if(recvd=='R'){
      fspeed+=25;
      bspeed-=25;
      rspeed-=25;
      lspeed+=25;
      write_motors();
    }
    else if(recvd=='L'){
      fspeed-=25;
      bspeed+=25;
      rspeed+=25;
      lspeed-=25;
      write_motors();
    }
    
    
    if(recvd!='F' && recvd!='R' && recvd!='B' && recvd!='L' && recvd!='T' && recvd!='E'){
      tspeed=(int)((fspeed+bspeed+rspeed+lspeed)/4);
      fspeed=tspeed;
      bspeed=tspeed;
      rspeed=tspeed;
      lspeed=tspeed;
      write_motors();
    }
    
    //control condition
    if(fspeed>MAXSPEED || bspeed>MAXSPEED || rspeed>MAXSPEED || lspeed>MAXSPEED){
      fspeed=MAXSPEED;
      bspeed=MAXSPEED;
      rspeed=MAXSPEED;
      lspeed=MAXSPEED;
      write_motors();
    }

  }
  else if(danger_flag==1){ // unsafe to lift
    tspeed=(int)((fspeed+bspeed+rspeed+lspeed)/4);
    fspeed=tspeed;
    bspeed=tspeed;
    rspeed=tspeed;
    lspeed=tspeed;
    write_motors();
    
    while(tspeed!=0){
      fspeed-=5;
      bspeed-=5;
      rspeed-=5;
      lspeed-=5;
      write_motors();
    }
    stop_motors();
  }
//  Serial.print("f ");
//  Serial.print(fspeed);
//  Serial.print(" b ");
//  Serial.print(bspeed);
//  Serial.print(" r ");
//  Serial.print(rspeed);
//  Serial.print(" l ");
//  Serial.println(lspeed);
  //delay(100);
  attachInterrupt(1,data_incoming,RISING);
}//routine

void write_motors(){
  motor1.writeMicroseconds(fspeed);
  motor2.writeMicroseconds(bspeed);
  motor3.writeMicroseconds(rspeed);
  motor4.writeMicroseconds(lspeed);
}

void stop_motors(){
  motor1.writeMicroseconds(0);
  motor1.writeMicroseconds(0);
  motor1.writeMicroseconds(0);
  motor1.writeMicroseconds(0);
}

void warnme(){
  pinMode(13,OUTPUT);
  digitalWrite(13,HIGH);
  delay(250);
  digitalWrite(13,LOW);
  delay(250);
  digitalWrite(13,HIGH);
  delay(250);
  digitalWrite(13,LOW);
  delay(250);
    digitalWrite(13,HIGH);
  delay(250);
  digitalWrite(13,LOW);
  delay(250);
    digitalWrite(13,HIGH);
  delay(250);
  digitalWrite(13,LOW);
  delay(250);
}
