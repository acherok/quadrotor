#include <Wire.h>
#include "Adafruit_BMP085.h"
#include <Servo.h>

Servo motor1;
Servo motor2;
Servo motor3;
Servo motor4;

#define DESIRED_HEIGHT 3 //3 meters
#define GROUND_HEIGHT 0 //0 meters

/*Pin cofigurations*/
#define MOTOR_PIN1 8
#define MOTOR_PIN2 9
#define MOTOR_PIN3 10
#define MOTOR_PIN4 11

#define ACC_PIN_X 0
#define ACC_PIN_Y 1
#define ACC_PIN_Z 2

#define ERROR_HEIGHT 1 //1 inch
#define ERROR_ANGLE 3  //3 degrees

#define MIN_SPEED 1000
#define MAX_SPEED 2000
/*******Gyro global variables************/
int gyro_x;
int gyro_y;
int gyro_z;
int cal_gyro_x;
int cal_gyro_y;
int cal_gyro_z;
int set_gyro_roll; // roll ~ x
int set_gyro_pitch; // pitch ~ y
int set_gyro_yaw; // yaw ~ z

/*******Baro global variables************/
Adafruit_BMP085 bmp;
int alt;
int height;
int set_alt;

/*******Accelerometer global variables******/
int acc_x;
int acc_y;
int acc_z;
int cal_acc_x; //roll
int cal_acc_y; //pitch
int set_acc_roll;
int set_acc_pitch;

/*******Angle global variables**************/
int roll;
int pitch;
int yaw;
int set_roll;
int set_pitch;
int set_yaw;

/***************Flags********************/
int flag;

/***************motors********************/
int universal_speed;


void setup(){
  Wire.begin();
  Serial.begin(9600);
  
  //Gyrometer
  init_gyro(); //√
  read_gyro(); //√
  calibrate_gyro(); //√
  set_base_gyro(); //√
  
  //Barometer
  init_baro(); //√
  read_baro(); //√
  calibrate_baro(); //√
  set_base_baro(); //√
  
  //Accelerometer
  read_acc(); //√
  calibrate_acc(); //√
  set_base_acc(); //√
  
  delay(10); // relaxation time
  
  //Angle
  calc_angle(); //√
  set_base_angle(); //√
  
  //Pinmode definitions
  motor1.attach(MOTOR_PIN1);
  motor2.attach(MOTOR_PIN2);
  motor3.attach(MOTOR_PIN3);
  motor4.attach(MOTOR_PIN4);
  
  //Motors
  warm_up(); //√
  turn_off(); //√
  
  set_flag(2); // 2= ready; 3=height attained √
}

void loop(){
  init_motors(); //turn on motors √
  int roll_angle=0;
  int pitch_angle=0;
  int yaw_angle=0;
  while(read_flag() != 3){ // √
    
    //Height maintenance
    read_baro();
    calibrate_baro();
    if(DESIRED_HEIGHT - read_height() >= ERROR_HEIGHT){ // √
      speed_up(); // √
    }
    else if(read_height() - DESIRED_HEIGHT >= ERROR_HEIGHT){ //√
      speed_down(); // √
    }
    else{
      hold_speed(); //√
      set_flag(3);
    }
    
    //Angle Measurement
    read_acc();
    calibrate_acc();
    
    read_gyro();
    calibrate_gyro();
    
    roll_angle = calc_roll_angle(); // √
    pitch_angle = calc_pitch_angle(); // √
    yaw_angle = calc_yaw_angle(); // √
    
    //Roll angle equlibrium control
    if(roll_angle - set_roll >= ERROR_ANGLE){
      speedup_m01(); // √
      speedup_m02(); // √
      speeddown_m03(); // √
      speeddown_m04(); // √
    }
    else if(roll_angle - set_roll <= ERROR_ANGLE){
      speedup_m03(); // √
      speedup_m04(); // √
      speeddown_m01(); // √
      speeddown_m02(); // √
    }
    else{
      continue;
    }
    
    //Pitch angle equlibrium control
    if(pitch_angle - set_pitch >= ERROR_ANGLE){
      speedup_m01();
      speedup_m04();
      speeddown_m03();
      speeddown_m02();
    }
    else if(pitch_angle - set_pitch <= ERROR_ANGLE){
      speedup_m02();
      speedup_m03();
      speeddown_m01();
      speeddown_m04();
    }
    else{
      continue;
    }
    
  }
  
  delay(5000);
  speed_down();
    
  //Angle Measurement
  read_acc();
  calibrate_acc();
  
  read_gyro();
  calibrate_gyro();
  
  roll_angle = calc_roll_angle();
  pitch_angle = calc_pitch_angle();
  yaw_angle = calc_yaw_angle();
  
  //Roll angle equlibrium control
  if(roll_angle - set_roll >= ERROR_ANGLE){
    speedup_m01();
    speedup_m02();
    speeddown_m03();
    speeddown_m04();
  }
  else if(roll_angle - set_roll <= ERROR_ANGLE){
    speedup_m03();
    speedup_m04();
    speeddown_m01();
    speeddown_m02();
  }
  else{
    //continue;
  }
  
  //Pitch angle equlibrium control
  if(pitch_angle - set_pitch >= ERROR_ANGLE){
    speedup_m01();
    speedup_m04();
    speeddown_m03();
    speeddown_m02();
  }
  else if(pitch_angle - set_pitch <= ERROR_ANGLE){
    speedup_m02();
    speedup_m03();
    speeddown_m01();
    speeddown_m04();
  }
  else{
    //continue;
  }
  
  read_baro();
  calibrate_baro();
  if(GROUND_HEIGHT - read_height() >= ERROR_HEIGHT){
    speed_down();
  }
  else if(read_height() - GROUND_HEIGHT >= ERROR_HEIGHT){
    speed_down();
  }
  else{
    turn_off();
    digitalWrite(7,HIGH);
  }
}

/**********Initializing gyro********/
#define GYRO 0x69
#define G_SMPLRT_DIV 0x15
#define G_DLPF_FS 0x16
#define G_INT_CFG 0x17
#define G_PWR_MGM 0x3E
#define G_TO_READ 8

void init_gyro(){
  writeTo(GYRO, G_PWR_MGM, 0x00);
  writeTo(GYRO, G_SMPLRT_DIV, 0x07);
  writeTo(GYRO, G_DLPF_FS, 0x1E);
  writeTo(GYRO, G_INT_CFG, 0x00);
}

/***********Reading gyro*************/
int g_offx=0;//120
int g_offy=0;//20
int g_offz=0;//93

void read_gyro(){
  int regAddress=0x1B;
  //int temp, x,y,z;
  int result[4];
  byte buff[G_TO_READ];
  readFrom(GYRO, regAddress, G_TO_READ, buff);
  result[0] = ((buff[2] << 8) | buff[3]) + g_offx;
  result[1] = ((buff[4] << 8) | buff[5]) + g_offy;
  result[2] = ((buff[6] << 8) | buff[7]) + g_offz;
  result[3] = (buff[0] << 8) | buff[1]; // temperature
  
  gyro_x = result[0]/14.375;
  gyro_y = result[1]/14.375;
  gyro_z = result[2]/14.375;
  
//  Serial.print(gyro_x+" "+gyro_y+" "+gyro_z+" \n");
}

/************writeTo function*************/
void writeTo(int DEVICE, byte address, byte val){
  Wire.beginTransmission(DEVICE);
  Wire.write(address);
  Wire.write(val);
  Wire.endTransmission();
}

/************readFrom function***************/
void readFrom(int DEVICE, byte address, int num, byte buff[]){
  Wire.beginTransmission(DEVICE);
  Wire.write(address);
  Wire.endTransmission();
  
  Wire.beginTransmission(DEVICE);
  Wire.requestFrom(DEVICE, num);
  
  int i=0;
  while(Wire.available()){
    buff[i]=Wire.read();
    i++;
  }
  Wire.endTransmission();
}

/***********calibrate_gyro********************/
void calibrate_gyro(){
  int reads_x=0;
  int reads_y=0;
  int reads_z=0;
    
  int i=0;
  for(i=0;i<15;i++){
    reads_x = reads_x + gyro_x;
    reads_y = reads_y + gyro_y;
    reads_z = reads_z + gyro_z;
    delayMicroseconds(10);
    read_gyro();
  }
  cal_gyro_x = reads_x / 16;
  cal_gyro_y = reads_y / 16;
  cal_gyro_z = reads_z / 16;
}

/***********setting base values of gyro*************/
void set_base_gyro(){
  set_gyro_roll = cal_gyro_x;
  set_gyro_pitch = cal_gyro_y;
  set_gyro_yaw = cal_gyro_z;
}

/*----------------------------------------------*/
/*************Initializing barometer*************/
void init_baro(){
  bmp.begin();
}

/*************Read barometer*********************/
void read_baro(){
  //alt = bmp.readAltitude(); //in meters
}

/*************Calibrate barometer*********************/
void calibrate_baro(){
  int reads_alt=0;
  int i=0;
  for(i=0;i<15;i++){
    reads_alt = reads_alt + alt;
    delayMicroseconds(10);
    read_baro();
  }
  height = reads_alt/16;
}

/*************setting base values of barometer********/
void set_base_baro(){
  set_alt = height;
}

/*************reading accelerometer********/
void read_acc(){
  acc_x = analogRead(ACC_PIN_X);
  acc_y = analogRead(ACC_PIN_Y);
  acc_z = analogRead(ACC_PIN_Z);
}

/*************calibrating accelerometer********/
void calibrate_acc(){
  int reads_x=0;
  int reads_y=0;
  int reads_z=0;
  int i=0;
  double fXg = 0;
  double fYg = 0;
  double fZg = 0;
  const float alpha = 0.5;
  
  for(i=0;i<15;i++){
    reads_x = acc_x;
    reads_y = acc_y;
    reads_z = acc_z;
    fXg = reads_x * alpha + (fXg*(1-alpha));
    fYg = reads_y * alpha + (fYg*(1-alpha));
    fZg = reads_z * alpha + (fZg*(1-alpha));
    read_acc();
  }
  cal_acc_x = (atan2(-fYg, fZg)*180.0)/M_PI;
  cal_acc_y = (atan2(fXg, sqrt(fYg*fYg + fZg*fZg))*180.0)/M_PI;
}
  
/*************setting base values of accelerometer********/  
void set_base_acc(){
  set_acc_roll = cal_acc_x;
  set_acc_pitch = cal_acc_y;
}

/***********calculate angle ***************************/
void calc_angle(){
  int trust_factor=0.93;
  roll = cal_acc_x*(1-trust_factor) + cal_gyro_x*trust_factor;
  pitch = cal_acc_y*(1-trust_factor) + cal_gyro_y*trust_factor;
  yaw = cal_gyro_z;
}

/***********set base values of angles******************/
void set_base_angle(){
  set_roll = roll;
  set_pitch = pitch;
  set_yaw = yaw;
}

/***************Motor run function**********************/
 void run_motor(int pin, int speed1, int increment){
   int k = map((speed1+increment), MIN_SPEED, MAX_SPEED, 0, 180);
   motor1.write(k);
   universal_speed = speed1+increment;
 }
 
 
/**************Warming up motors***********************/
 void warm_up(){
   //to activate
   run_motor(MOTOR_PIN1,MIN_SPEED,0);
   run_motor(MOTOR_PIN2,MIN_SPEED,0);
   run_motor(MOTOR_PIN3,MIN_SPEED,0);
   run_motor(MOTOR_PIN4,MIN_SPEED,0);
   
   delay(2000);
   
   run_motor(MOTOR_PIN1,MAX_SPEED,0);
   run_motor(MOTOR_PIN2,MAX_SPEED,0);
   run_motor(MOTOR_PIN3,MAX_SPEED,0);
   run_motor(MOTOR_PIN4,MAX_SPEED,0);
 }
 
 /*************Turning off motors**********************/
 void turn_off(){
   motor1.write(0);
   motor2.write(0);
   motor3.write(0);
   motor4.write(0);
 }
 
 /***************Set flag ************************/
 void set_flag(int k){
   flag = k;
 }
 
 /**************Initialize Motors********************/
 void init_motors(){
   run_motor(MOTOR_PIN1,MIN_SPEED,0);
   run_motor(MOTOR_PIN2,MIN_SPEED,0);
   run_motor(MOTOR_PIN3,MIN_SPEED,0);
   run_motor(MOTOR_PIN4,MIN_SPEED,0);
 }

/**************read flag********************/ 
int read_flag(){
   return flag;
 }
 
/**************read height********************/ 
int read_height(){
   return height;
 }
 
/**************speed up********************/ 
int speed_up(){
  run_motor(MOTOR_PIN1,MIN_SPEED,10);
  run_motor(MOTOR_PIN2,MIN_SPEED,10);
  run_motor(MOTOR_PIN3,MIN_SPEED,10);
  run_motor(MOTOR_PIN4,MIN_SPEED,10);
}
 
/**************speed down********************/ 
int speed_down(){
  run_motor(MOTOR_PIN1,MIN_SPEED,-10);
  run_motor(MOTOR_PIN2,MIN_SPEED,-10);
  run_motor(MOTOR_PIN3,MIN_SPEED,-10);
  run_motor(MOTOR_PIN4,MIN_SPEED,-10);
} 
 
 /***************hold motors speed********************/
void hold_speed(){
   run_motor(MOTOR_PIN1,universal_speed,0);
   run_motor(MOTOR_PIN2,universal_speed,0);
   run_motor(MOTOR_PIN3,universal_speed,0);
   run_motor(MOTOR_PIN4,universal_speed,0);
}

/***************roll angle********************/
int calc_roll_angle(){
   return roll;
}

/***************pitch angle********************/
int calc_pitch_angle(){
   return pitch;
}

/***************yaw angle********************/
int calc_yaw_angle(){
   return yaw;
}

/***************speed up individual motors********************/
void speedup_m01(){
  run_motor(MOTOR_PIN1,universal_speed,4);
}
void speedup_m02(){
  run_motor(MOTOR_PIN2,universal_speed,4);
}
void speedup_m03(){
  run_motor(MOTOR_PIN3,universal_speed,4);
}
void speedup_m04(){
  run_motor(MOTOR_PIN4,universal_speed,4);
}

/***************speed down individual motors********************/
void speeddown_m01(){
  run_motor(MOTOR_PIN1,universal_speed,-4);
}
void speeddown_m02(){
  run_motor(MOTOR_PIN2,universal_speed,-4);
}
void speeddown_m03(){
  run_motor(MOTOR_PIN3,universal_speed,-4);
}
void speeddown_m04(){
  run_motor(MOTOR_PIN4,universal_speed,-4);
}

// So done here!! phew
