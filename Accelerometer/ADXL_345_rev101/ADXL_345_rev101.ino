#include <Wire.h> 

void setup() 
{ 
  Serial.begin(115200); 
  Wire.begin(); 
   
  Serial.println("Demo started, initializing sensors"); 
  AccelerometerInit(); 
  Serial.println("Sensors have been initialized"); 
} 

void AccelerometerInit() 
{ 
  Wire.beginTransmission(0xE5); // address of the accelerometer 
  // reset the accelerometer 
  Wire.write(0x3A);
  delay(1);
  Wire.write(0x2D);
  delay(1);
  Wire.write((1<<3));
  delay(1);
  Wire.endTransmission();
} 

void AccelerometerRead() 
{ 
  Wire.beginTransmission(0xE5); // address of the accelerometer 
  Wire.write(0x3A); // set read pointer to data 
  Wire.endTransmission(); 
  Wire.requestFrom(0x32, 6); 
   
  // read in the 3 axis data, each one is 16 bits 
  // print the data to terminal 
  Serial.print("Accelerometer: X = "); 
  short data = Wire.read(); 
  data += Wire.read() << 8; 
  Serial.print(data); 
  Serial.print(" , Y = "); 
  data = Wire.read(); 
  data += Wire.read() << 8; 
  Serial.print(data); 
  Serial.print(" , Z = "); 
  data = Wire.read(); 
  data += Wire.read() << 8; 
  Serial.print(data); 
  Serial.println(); 
} 


void loop() 
{ 
  AccelerometerRead(); 
   
  delay(500); // slow down output   
}
