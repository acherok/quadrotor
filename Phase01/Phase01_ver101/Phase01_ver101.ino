#include <Wire.h>
#include <Servo.h>

//CONSTANTS DEFINITIONS & PIN COFIGURATIONS
// Motors
#define MAX_SPEED 2000
#define MIN_SPEED 1000

#define MOTOR_PIN1 8
#define MOTOR_PIN2 9
#define MOTOR_PIN3 10
#define MOTOR_PIN4 11

//gyrometer
#define GYRO_ADDRESS 0x69
#define SMPLRT_DIV 0x15
#define DLPF_FS 0x16
#define INT_CFG 0x17
#define PWR_MGM 0x3E
#define TO_READ 8

int offx = 99.5;
int offy = -45.0;
int offz = -29.7;

//OBJECTS CREATe
Servo motor1;
Servo motor2;
Servo motor3;
Servo motor4;

// DEVICES INITIALIZATION
// Initialize gyrometer
void init_gyro(){
  writeTo(GYRO_ADDRESS, PWR_MGM, 0x00);
  writeTo(GYRO_ADDRESS, SMPLRT_DIV, 0x07); 
  writeTo(GYRO_ADDRESS, DLPF_FS, 0x1E); 
  writeTo(GYRO_ADDRESS, INT_CFG, 0x00);
}
// Initialize barometer
// Initialize motors
void init_motors(){
  int mot1_inp, mot2_inp, mot3_inp, mot4_inp;
  
  mot1_inp = 0;
  mot2_inp = 0;
  mot3_inp = 0;
  mot4_inp = 0;
  
  mot1_inp = map(mot1_inp,0,1023,MIN_SPEED, MAX_SPEED);
  mot2_inp = map(mot2_inp,0,1023,MIN_SPEED, MAX_SPEED);
  mot3_inp = map(mot3_inp,0,1023,MIN_SPEED, MAX_SPEED);
  mot4_inp = map(mot4_inp,0,1023,MIN_SPEED, MAX_SPEED);
  
  motor1.write(mot1_inp);
  motor2.write(mot1_inp);
  motor3.write(mot1_inp);
  motor4.write(mot1_inp);
  
  delay(3000);
  
  mot1_inp = 1023;
  mot2_inp = 1023;
  mot3_inp = 1023;
  mot4_inp = 1023;
  
  mot1_inp = map(mot1_inp,0,1023,MIN_SPEED, MAX_SPEED);
  mot2_inp = map(mot2_inp,0,1023,MIN_SPEED, MAX_SPEED);
  mot3_inp = map(mot3_inp,0,1023,MIN_SPEED, MAX_SPEED);
  mot4_inp = map(mot4_inp,0,1023,MIN_SPEED, MAX_SPEED);
  
  motor1.write(mot1_inp);
  motor2.write(mot1_inp);
  motor3.write(mot1_inp);
  motor4.write(mot1_inp);
 
 return ; 
}

// Write to I2C Device
void writeTo(int device, byte address, byte val) {
   Wire.beginTransmission(device); //start transmission to device 
   Wire.write(address);        // write register address
   Wire.write(val);        // write value to write
   Wire.endTransmission(); //end transmission
}

// Read from I2C Device
void readFrom(int device, byte address, int num, byte buff[]) {
  Wire.beginTransmission(device); //start transmission to device 
  Wire.write(address);        //writes address to read from
  Wire.endTransmission(); //end transmission
  
  Wire.beginTransmission(device); //start transmission to device
  Wire.requestFrom(device, num);    // request 6 bytes from device
  
  int i = 0;
  while(Wire.available())    //device may write less than requested (abnormal)
  { 
    buff[i] = Wire.read(); // read a byte
    i++;
  }
  Wire.endTransmission(); //end transmission
}


// READ from DEVICES
// Read from accelerometer
// Read from gyrometer
void getGyroscopeData()
{
  int regAddress = 0x1B;
  int temp, x, y, z;
  byte buff[TO_READ];
  char str[50]; // 50 should be enough to store all the data from the gyro
  
  readFrom(GYRO_ADDRESS, regAddress, TO_READ, buff); //read the gyro data from the ITG3200
  
  temp = (buff[0] << 8) | buff[1];
  x = ((buff[2] << 8) | buff[3]) + offx;
  y = ((buff[4] << 8) | buff[5]) + offy;
  z = ((buff[6] << 8) | buff[7]) + offz;

  //we write the x y z values as a string to the serial port
  sprintf(str, "%d,%d,%d,%d,", temp, x, y, z);  
  Serial.print(str);
  Serial.println(" ");
  
}
// Read from barometer

// DEVICES CALIBRATION
// Accelerometer calibration
// Gyrometer calibration
// Barometer calibration

void setup(){
  Wire.begin();
  delay(100);
  
  Serial.begin(9600);
  
  init_gyro();
  //init_baro();
  
  //attach motors
  motor1.attach(MOTOR_PIN1);
  motor2.attach(MOTOR_PIN2);
  motor3.attach(MOTOR_PIN3);
  motor4.attach(MOTOR_PIN4);
  //initialize motors
  init_motors();
  init_gyro();
  
}

void loop(){
  getGyroscopeData();
  
}
