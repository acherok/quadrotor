#include <Wire.h>
#include <Servo.h>

#define MAX_SPEED 2000
#define MIN_SPEED 1000

Servo motor1;
Servo motor2;
Servo motor3;
Servo motor4;

// DEVICES INITIALIZATION
// Initialize gyrometer
// Initialize barometer
// Initialize motors
void init_motors(){
  int mot1_inp, mot2_inp, mot3_inp, mot4_inp;
  
  mot1_inp = 0;
  mot2_inp = 0;
  mot3_inp = 0;
  mot4_inp = 0;
  
  mot1_inp = map(mot1_inp,0,1023,MIN_SPEED, MAX_SPEED);
  mot2_inp = map(mot2_inp,0,1023,MIN_SPEED, MAX_SPEED);
  mot3_inp = map(mot3_inp,0,1023,MIN_SPEED, MAX_SPEED);
  mot4_inp = map(mot4_inp,0,1023,MIN_SPEED, MAX_SPEED);
  
  motor1.write(mot1_inp);
  motor2.write(mot1_inp);
  motor3.write(mot1_inp);
  motor4.write(mot1_inp);
  
  delay(3000);
  
  mot1_inp = 1023;
  mot2_inp = 1023;
  mot3_inp = 1023;
  mot4_inp = 1023;
  
  mot1_inp = map(mot1_inp,0,1023,MIN_SPEED, MAX_SPEED);
  mot2_inp = map(mot2_inp,0,1023,MIN_SPEED, MAX_SPEED);
  mot3_inp = map(mot3_inp,0,1023,MIN_SPEED, MAX_SPEED);
  mot4_inp = map(mot4_inp,0,1023,MIN_SPEED, MAX_SPEED);
  
  motor1.write(mot1_inp);
  motor2.write(mot1_inp);
  motor3.write(mot1_inp);
  motor4.write(mot1_inp);
 
 return ; 
}

// WRITE to DEVICES
// Write to gyrometer
// Write to barometer

// READ from DEVICES
// Read from accelerometer
// Read from gyrometer
// Read from barometer

// DEVICES CALIBRATION
// Accelerometer calibration
// Gyrometer calibration
// Barometer calibration

void setup(){
  Wire.begin();
  delay(100);
  
  Serial.begin(9600);
  
  init_gyro();
  init_baro();
  
  init_motors();
}

void loop(){
  
