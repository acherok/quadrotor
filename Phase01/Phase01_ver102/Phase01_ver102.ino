#include <Wire.h>
#include <Servo.h>

/*Pin Descriptions:
    Motor 01- 8
    Motor 02- 9
    Motor 03- 10
    Motor 04- 11
    
    Acc_X- A0
    Acc_Y- A1
    Acc_Z- A2
    
    Gyro and Baro- SDA and SCL
*/
#define MOTOR_PIN01 8
#define MOTOR_PIN02 9
#define MOTOR_PIN03 10
#define MOTOR_PIN04 11
#define ACC_X_PIN 0
#define ACC_Y_PIN 1
#define ACC_Z_PIN 2


void setup(){
  Wire.begin();
  delay(100);
  
  Serial.begin(9600);
  
  init_gyro();
  init_baro();
  //init_compass(); // For later use
  
  delay(50);
  
  read_gyro();
  set_ground_gyro();
  
  read_baro();
  set_ground_baro();
  
  read_acc();
  set_ground_acc();
  
  attach_motors();
  init_motors();
  
}

void loop(){
  /* Now what has to be done is to calculate the height of quad.
  Increase the throttle, read sensors, calculate roll, pitch and yaw
  adjust the speed of motors to nullify roll or pitch or yaw
  continue until desired height is attained.
  */
}
