#define THROTTLE 4
#define BRAKE 5
#define FRONT 6
#define BACK 7
#define RIGHT 8
#define LEFT 9

#define STOP 10
#define START 11

void setup(){      
  pinMode(2, OUTPUT);
  
  pinMode(THROTTLE,INPUT);
  pinMode(BRAKE,INPUT);
  pinMode(FRONT,INPUT);
  pinMode(BACK,INPUT);
  pinMode(RIGHT,INPUT);
  pinMode(LEFT,INPUT);
  pinMode(STOP,INPUT);
  pinMode(START,INPUT);
  
  digitalWrite(THROTTLE,HIGH);
  digitalWrite(BRAKE,LOW);
  digitalWrite(FRONT,LOW);
  digitalWrite(BACK,LOW);
  digitalWrite(RIGHT,LOW);
  digitalWrite(LEFT,LOW);
  digitalWrite(STOP,LOW);
  digitalWrite(START,LOW);
  Serial.begin(9600);
}

void loop(){
  int i1,i2,i3,i4,i5,i6,i7,i8;
  
  i1=digitalRead(THROTTLE);
  i2=digitalRead(BRAKE);
  
  i3=digitalRead(FRONT);
  i4=digitalRead(BACK);
  i5=digitalRead(LEFT);
  i6=digitalRead(RIGHT);
  
  i7=digitalRead(STOP);
  i8=digitalRead(START);
  
  
  //======= START OR STOP
  if(i8==LOW){
    rf_send('O'); //START
  }
  else if(i7==LOW){
    rf_send('X'); //STOP
  }
  
  //======== THROTTLE OR BRAKE
  if(i1==LOW){
    rf_send('T');
  }
  else if(i2==LOW){
    rf_send('E');
  }
  //========= FRONT OR BACK
  if(i3==LOW){
    rf_send('F');
  }
  
  else if(i4==LOW){
    rf_send('B');
  }
  //=====LEFT OR RIGHT
  
  if(i5==LOW){
    rf_send('L');
  }
  
  else if(i6==LOW){
    rf_send('R');
  }
  delay(1);
}

void rf_send(byte input){
  int i;
 // Serial.println(input);
  for(i=0; i<20; i++){
  digitalWrite(2, HIGH);
  delayMicroseconds(500);
  digitalWrite(2, LOW);
  delayMicroseconds(500);
}

  digitalWrite(2, HIGH);
  delayMicroseconds(3000);
  digitalWrite(2, LOW);
  delayMicroseconds(500);
  
    
  for(i=0; i<8; i++){
  if(bitRead(input,i)==1)
  digitalWrite(2, HIGH);
  else
  digitalWrite(2, LOW);
  delayMicroseconds(500);
  
  if(bitRead(input,i)==1)
  digitalWrite(2, LOW);
  else
  digitalWrite(2, HIGH);
  delayMicroseconds(500);
}

  
  digitalWrite(2, LOW);
}



