#include <Servo.h>

#define MAX_SIGNAL 1200
#define MIN_SIGNAL 700

#define MOTOR_PIN1 9
#define MOTOR_PIN2 10
#define MOTOR_PIN3 5
#define MOTOR_PIN4 6

Servo motor1, motor2, motor3, motor4;

void setup() {
  Serial.begin(9600);
  Serial.println("Program begin...");
  Serial.println("This program will calibrate the ESC.");

  motor1.attach(MOTOR_PIN1);
  motor2.attach(MOTOR_PIN2);
  motor3.attach(MOTOR_PIN3);
  motor4.attach(MOTOR_PIN4);

  Serial.println("Now writing maximum output.");
  Serial.println("Turn on power source, then wait 2 seconds and press any key.");
  warnme();
  delay(2000);
  motor1.writeMicroseconds(MAX_SIGNAL);
  motor2.writeMicroseconds(MAX_SIGNAL);
  motor3.writeMicroseconds(MAX_SIGNAL);
  motor4.writeMicroseconds(MAX_SIGNAL);

  // Wait for input
  delay(6000);
  // Send min output
  Serial.println("Sending minimum output");
  motor1.writeMicroseconds(MIN_SIGNAL);
  motor2.writeMicroseconds(MIN_SIGNAL);
  motor3.writeMicroseconds(MIN_SIGNAL);
  motor4.writeMicroseconds(MIN_SIGNAL);
}

void loop() {
  int readdata=-1;
  while(1){
    if(Serial.available()){
      readdata=Serial.read();
      Serial.println(readdata);
      if(readdata==1 || readdata==2 || readdata==3 || readdata==4 || readdata==0){
        break;
      }
    }
    else
      Serial.println("Serial not available!! Recheck connections");
  }
  
  switch(readdata){
    case 1: movefwd();
            break;
    case 2: movebkwd();
            break;
    case 3: movelft();
            break;
    case 4: moveryt();
            break;
    case 0: stopme();
    default: break;
  }
}

void movefwd(){
  motor1.writeMicroseconds(1150);
  motor2.writeMicroseconds(1250);
  motor3.writeMicroseconds(1250);
  motor4.writeMicroseconds(1150);
}

void movebkwd(){
  motor1.writeMicroseconds(1250);
  motor2.writeMicroseconds(1150);
  motor3.writeMicroseconds(1150);
  motor4.writeMicroseconds(1250);
}

void moveryt(){
  motor1.writeMicroseconds(1150);
  motor2.writeMicroseconds(1250);
  motor3.writeMicroseconds(1250);
  motor4.writeMicroseconds(1250);
}

void movelft(){
  motor1.writeMicroseconds(1250);
  motor2.writeMicroseconds(1250);
  motor3.writeMicroseconds(1150);
  motor4.writeMicroseconds(1150);
}

void stopme(){
  motor1.writeMicroseconds(MIN_SIGNAL);
  motor2.writeMicroseconds(MIN_SIGNAL);
  motor3.writeMicroseconds(MIN_SIGNAL);
  motor4.writeMicroseconds(MIN_SIGNAL);
}

void warnme(){
  pinMode(13,OUTPUT);
  digitalWrite(13,HIGH);
  delay(250);
  digitalWrite(13,LOW);
  delay(250);
  digitalWrite(13,HIGH);
  delay(250);
  digitalWrite(13,LOW);
  delay(250);
    digitalWrite(13,HIGH);
  delay(250);
  digitalWrite(13,LOW);
  delay(250);
    digitalWrite(13,HIGH);
  delay(250);
  digitalWrite(13,LOW);
  delay(250);
}
